'use strict';
// search list directive
skyControllers.directive("searchList", [function() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: "/sky/partials/directives/search-list.html",
            scope: {
                activeItem: '=',
                searchList: '=',
                totalResults: '='
            },
            link: function(scope) {
                // setActiveItem() method is used for the mouseenter event
                scope.setActiveItem = function(index) {
                    scope.activeItem = index;
                    // emit event to parent scope to update the activeItem
                    scope.$emit("setActiveItem", {item: index});
                }
            }
        };
    }
]);
