'use strict';
// header directive
skyControllers.directive("headerContainer", [function() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: "/sky/partials/directives/header-container.html"
        };
    }
]);
