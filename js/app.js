'use strict';

// Declare app level module which depends on filters, and services
var sky = angular.module('sky', [
    'ngRoute',
    'sky.filters',
    'sky.services',
    'sky.controllers',
    'sky.directives'
]);

sky.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $routeProvider.when('/homepage', {templateUrl: 'partials/homepage.html', controller: 'homepageController'});
    $routeProvider.when('/all-movies', {templateUrl: 'partials/all-movies.html', controller: 'allMoviesController'});
    $routeProvider.when('/movie/:movieId', {templateUrl: 'partials/movie-page.html', controller: 'moviePageController'});
    $routeProvider.otherwise({redirectTo: '/homepage'});

    // use html5 style paths if possible and #! if not
    $locationProvider.html5Mode(true).hashPrefix("!");
}]);

// declare global modules that will be used as containers for filters, services, controllers and directives
var skyFilters = angular.module("sky.filters", []);
var skyServices = angular.module("sky.services", []);
var skyControllers = angular.module("sky.controllers", []);
var skyDirectives = angular.module("sky.directives", []);