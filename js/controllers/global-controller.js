'use strict';
// global controller - usually used so that we don't attach global stuff directly into the $rootScope
skyControllers.controller("globalController", ["$scope", "$window", function($scope, $window) {
        $scope.displaySearch = true;

        // this isn't the greatest piece of code in history but will serve the purpose
        function childOf(c, p){
            while((c=c.parentNode)&&c!==p);
            return !!c;
        }
        // same as above
        $window.onclick = function(e) {
            var target = e.target;
            if(!target) return;
            var parent = document.getElementById("search-container");
            if (childOf(target, parent)) {
                $scope.displaySearch = true;
                $scope.$apply();
            }
            else {
                $scope.displaySearch = false;
                $scope.$apply();
            }
        };
    }
]);