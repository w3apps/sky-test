'use strict';
// search controller
skyControllers.controller("searchController", ["$scope", "$http", "$timeout", "$location", function($scope, $http, $timeout, $location) {
        // define some stuff
        $scope.searchKeyword = "";
        $scope.searchList = []
        $scope.totalResults = 0;
        $scope.showSearchList = false;
        $scope.activeItem = null;

        var promise = null;
        var letterCount = 3; // number of letters required to show the list
        var ajaxTimeout = 500; // delay in miliseconds until the AJAX is fired after a letter was entered

        // showResults() method is triggered on every keypress
        $scope.showResults = function(ev) {
            // don't do anything if arrow up or arrow down
            if (ev.which !== 38 && ev.which !== 40) {
                $timeout.cancel(promise);
                // if there are more than 2 letters in the searchbox make the AJAX call
                if ($scope.searchKeyword.length >= letterCount) {
                    // use a small timeout for the ajax call so that in case the user types
                    // 5 letters fast we only do 1 AJAX call at the end and cancel the previous ones
                    promise = $timeout(function() {
                        $http({
                            method: 'GET',
                            url: '/sky/data/results.json'
                        })
                        .success(function(data) {
                            var newList = [];
                            var c = 0;
                            for (var i = 0; i < data.length; i++) {
                                // if the current item matches the search add it to the new array
                                if (data[i].title.toLowerCase().indexOf($scope.searchKeyword) !== -1) {
                                    // add a test image to the object
                                    data[i].thumbnail = "test-image.png"
                                    // use a temp array so we don't trigger the digest cycle for every update of the scope
                                    newList.push(data[i]);
                                    c++;
                                }
                            }

                            // keep only 5 items
                            if (c > 5) {
                                newList = newList.slice(0, 5);
                            }

                            // show or hide the search list
                            if (c > 0) $scope.showSearchList = true;
                            else $scope.showSearchList = false;

                            // update the scope only once after we have the entire list
                            $scope.searchList = newList;
                            $scope.totalResults = c;
                        });
                    }, ajaxTimeout)
                }
                // hide the list if there aren't 3 letters
                else $scope.showSearchList = false;
            }

        }

        // navigateList() method is used for arrow navigation
        $scope.navigateList = function(ev) {
            // down arrow
            if (ev.which === 40) {
                if ($scope.activeItem === null) $scope.activeItem = 0;
                else if ($scope.activeItem < $scope.searchList.length-1) $scope.activeItem++;
                else  $scope.activeItem = null
            }
            // up arrow
            else if (ev.which === 38) {
                if ($scope.activeItem === null) $scope.activeItem = $scope.searchList.length-1;
                else if ($scope.activeItem > 0) $scope.activeItem--;
                else  $scope.activeItem = null

            }
            // enter key
            else if (ev.which === 13 && $scope.activeItem !== null) {
                $location.path("movie/"+$scope.searchList[$scope.activeItem].slug);
            }
        }

        // lisen to setActiveItem event received from search-list directive
        $scope.$on("setActiveItem", function(event, message) {
            $scope.activeItem = message.item;
            event.stopPropagation();
        })

    }
]);